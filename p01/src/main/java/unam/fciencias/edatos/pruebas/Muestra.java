package unam.fciencias.edatos.pruebas;

import java.util.Random;

/**
 * Clase que maneja la creacion y el correcto funcionamiento de las pruebas
 * @author Ariel Merino
 *
 */
public class Muestra {
    private int[] arreglo;
    private int repeticiones = 1;
    private int longitud = 1;
    private long resultado;

    /**
     * Constructor que crea una prueba donde se implementan algunos metodos que permiten el calculo
     * de un valor empleado para hacer pruebas sobre este.
     * @param longitud numero de muestras que tendra
     * @param repeticiones cantidad de veces que repetira la prueba
     */
    public Muestra(int longitud, int repeticiones){
        if (longitud > 1 && repeticiones > 1){
            this.longitud = longitud;
            this.repeticiones = repeticiones;
        }
        arreglo = new int[longitud];
        Random random = new Random();
        int a = random.nextInt(15);
        for (int i = 0; i < arreglo.length; i++) {
            this.arreglo[i] = i+ a;
        }
    }

    public long getResultado() {
        return resultado;
    }

    public int getLongitud(){
        return longitud;
    }

    /**
     * Se encarga de poner en la terminal los valores separados por espacio y al final dar un salto de linea
     */
    public void imprimeArreglo(){
        for (int i = 0; i < arreglo.length ; i++) {
            System.out.print(arreglo[i] + " ");
        }
        System.out.println("\n");
    }
    @Override
    public String toString() {
        return  "(" +longitud +", "+ resultado + ")";
    }

    /**
     * Calcula una media de tiempo que tarada en realizar una busqueda
     * @return promedio de las pruebas
     */
    public void generarPrueba(){
        int a = 0;
        long elapsedTime = 0;
        Random ran = new Random();
        while (a++ < repeticiones){
            long start = System.nanoTime();
            bb(ran.nextInt(longitud *2));
            long end = System.nanoTime();
            elapsedTime += end - start;
        }
        resultado= elapsedTime/repeticiones;
    }

    /**
     * Busqueda binaria, dobre un arreglo ordenado implementa la
     * @param busqueda el entero que se tratara de encontrar en el arreglo
     * @return la posicion donde fue hallado el valor
     */
    private int bb(int busqueda){
        return bb(0,longitud-1,busqueda);
    }
    private int bb(int topeIzquierdo, int topeDerecho, int busqueda) {
        if (topeDerecho >= 0 && arreglo[topeIzquierdo] <= busqueda && arreglo[topeDerecho] >= busqueda) {
            int mid = valorMedio(topeIzquierdo, topeDerecho);
            if (arreglo[mid] == busqueda) {
                return mid;
            } else if (arreglo[mid]< busqueda) {
                return bb(mid + 1, topeDerecho, busqueda);
            }
            return bb(topeIzquierdo, mid - 1, busqueda);
        }
        return -1;
    }

    /**
     * Metodo auxiliar que calcula la mitad entre dos numeros
     * @param minLimit el primer valor para la media
     * @param maxLimit el segundo valor que sera sumado
     * @return la media entre ambos parametros
     */
    private static int valorMedio(int minLimit, int maxLimit) {
        return (maxLimit + minLimit) / 2;
    }
/**
 * ---------DUDAS--------------
 * ¿El constructor esta bien que haga esas validaciones o deben hacerse en otra clase o en la implementacion
 * directamente?
 *
 * ¿Es correcto que en el constructor de la clase haga to do el calculo o no?
 *
 *
 * ¿Una mejor implementacion de busqueda binaria seria que desde el principio realice una validacion si el
 * elemento a buscar el menor que la posicion 0 del arreglo y que vea si el valor a buscar es mayor que la
 * ultima posicion del arreglo?
 *
 * ¿Vale la pena que mi metodo valor medio calcule directamente los maximos y minimos asi, o debo
 * verificar que minLimit sea menor que maxLimit?
 *
 *
 * ¿Como puedo configurar GitLab para subir lo que me hace falta?
 */
}
