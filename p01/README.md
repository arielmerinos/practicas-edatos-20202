# Práctica 01
## Algoritmos y su gráfica
El objetivo es poner en práctica los conocimientos del tiempo de reloj, TDA's y gráficas para el tiempo
de ejecución.


## Problemática
1. Necesitamos graficar el conjunto de puntos que corresponden al tiempo de ejecución 
del algoritmo _búsqueda binaria_ para los siguientes tamaños de ejemplares (todos enteros positivos):
*  2
*  5
*  10
*  100
*  1000
*  2000 
 
2. Para obtener una gráfica lo más cercana a la vida real, necesitamos repetir el experimiento (búsqueda binaria)
varias veces para tener un campo muestral que nos permita determinar la media de todos los experimentos porque para
los algoritmos existe: 
* El mejor caso
* El peor caso
* El caso promedio
 
Dado esto, para esta práctica nos importa graficar el **caso promedio**. El número de veces que repetiremos
el experimiento será de **10**.


## Desarrollo
El desarollo será en dos partes, la primer parte involucrará la implementación
de los TDA's y estructuras de datos que nos permitan obtener y almacenar los puntos (x, f(x))
donde:
* x es el tamaño del ejemplar
* f(x) el tiempo de ejecución para el algoritmo f con el ejemplar x

Para que el factor humano sea despreciable, deberás elegir al candidato a buscar, aleatoriamente.

La segunda parte involucra graficar los puntos generados *(x1, f(x1)), (x2, f(x2)),..., (xn, f(xn))*
usando JavaFX (OpenJFX), de tal forma que se asemeje a un plano cartesiano con componentes X, Y.

## Puntos a evaluar
Adicionalmente a los puntos mencionados en las **Especificaciones generales de entrega de prácticas**
se tomará en cuenta:

* Las razones de diseño de su solución, esto es, ¿por qué el uso de las estructuras y el diseño de los ADT's? (esto va en el README)
* La correcta visualización de los puntos
* Adecuada abstracción para llegar a la solución 


## Puntos extra
En esta práctica no es requerimiento implementar un conjunto de muestras unitarias, sin embargo, se deja
al alumno la libre decisión e implementación de estas para **la primer parte de la práctica** (JavaFX NO aplica).
Cuando esto suceda, además de pasar todas y cada una de las muestras, se evaluará cada una en correspondencia
con lo que prueba y si está correctamente probado.



